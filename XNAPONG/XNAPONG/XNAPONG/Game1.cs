using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SQUARE
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Random
        Random R = new Random(DateTime.Now.Second);

        //Paddle Instances
        Paddle p1 = new Paddle();

        //Ball instance
        Ball baller = new Ball();

        //Game Over Sound Effect
        SoundEffect GameOver;

        //Difficulty
        int Difficulty = 1;

        //Variable for pausing game
        bool Pause = true;

        //Create EnemyList
        List<EnemyBall> Enemies = new List<EnemyBall>();

        //Buffer zone for spawn
        int BufferZone = 500;

        //Set Highest High score
        int HighScore = SaveAndLoad.Load();

        //Game Over Variable
        bool gameover = false;


        //Audio
        SoundEffect ding;

        //Get users last keyboard state
        KeyboardState LastKeyboardState;

        //Font
        SpriteFont Font;
        SpriteFont PauseFont;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

             
             
            

                //Set to fullscreen
            graphics.IsFullScreen = true;
            /*
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
             */
            //Increase resolution if score is over 100000

                graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                //graphics.ApplyChanges();
            
        }

      
        protected override void Initialize()
        {

            base.Initialize();
        }

      
        protected override void LoadContent()
        {
           

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Fonts
            ////////////////////////////////////////////////////////////////////
            Font = Content.Load<SpriteFont>(@"Font");
            PauseFont = Content.Load<SpriteFont>(@"PauseFont");
            ////////////////////////////////////////////////////////////////////
            //TEXTURES
            ////////////////////////////////////////////////////////////////////
            p1.Texture = Content.Load<Texture2D>("player");
            baller.Texture = Content.Load<Texture2D>("ball");
            //Enemy textures
            foreach(EnemyBall enemy in Enemies)
            {
                enemy.Texture = Content.Load<Texture2D>("ballred");
            }
            ////////////////////////////////////////////////////////////////////
            //AUDIO
            ////////////////////////////////////////////////////////////////////
            baller.Ping = Content.Load<SoundEffect>("blip");
            GameOver = Content.Load<SoundEffect>("GAMEOVER");
            //Audio for enemies; Enemy starting location
            foreach (EnemyBall enemy in Enemies)
            {
                enemy.Ping = Content.Load<SoundEffect>("blip");

                do
                {
                enemy.Position.X = R.Next(0, GraphicsDevice.Viewport.Width);
                enemy.Position.Y = R.Next(0, GraphicsDevice.Viewport.Height);
                }
                while (enemy.Position.X >= p1.Position.X - BufferZone && enemy.Position.X <= p1.Position.X + p1.Texture.Width + BufferZone && enemy.Position.Y >= p1.Position.Y - p1.Texture.Height - BufferZone && enemy.Position.Y <= p1.Position.Y + p1.Texture.Height + BufferZone);
            }

            ding = Content.Load<SoundEffect>("ding");

            ////////////////////////////////////////////////////////////////////

            //Measures string to correct for screen center
            Vector2 StringVector = PauseFont.MeasureString("PAUSED");

            //Assign starting positions for player
            p1.Position.X = graphics.GraphicsDevice.Viewport.Width / 2 - (p1.Texture.Width / 2);
            p1.Position.Y = (graphics.GraphicsDevice.Viewport.Height / 2 - (p1.Texture.Height / 2)) - StringVector.Y;

            //Random Ball position
            do
            {
                baller.Position.X = R.Next(0, GraphicsDevice.Viewport.Width);
                baller.Position.Y = R.Next(0, GraphicsDevice.Viewport.Height);

            } while (baller.Position.X >= p1.Position.X - BufferZone && baller.Position.X <= p1.Position.X + p1.Texture.Width + BufferZone && baller.Position.Y >= p1.Position.Y - p1.Texture.Height - BufferZone && baller.Position.Y <= p1.Position.Y + p1.Texture.Height + BufferZone);


            


        }

       
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        //Update
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            //UPDATE Input
            PlayerInput();

            //If the game is not paused
            if (Pause == false)
            {
                

                //Update Paddle positions
                p1.Update(graphics);


                //Update ball position
                baller.Update(graphics, gameTime, p1);


                //Update enemies
                foreach (EnemyBall enemy in Enemies)
                {
                    enemy.Update(graphics, gameTime, p1, R);
                }

                //Collisions
                gameover = Collisions(baller);

                foreach (EnemyBall enemy in Enemies)
                {
                    gameover = Collisions(enemy);
                    if (gameover == true)
                    {
                        if (Paddle.Mute == false)
                        {
                            GameOver.Play();
                        }
                        
                        break;
                    }
                }


                //GAME OVER
                if (gameover == true)
                {
                    //Measures string to correct for screen center
                    Vector2 StringVector = PauseFont.MeasureString("PAUSED");

                    //Assign starting positions for player
                    p1.Position.X = graphics.GraphicsDevice.Viewport.Width / 2 - (p1.Texture.Width / 2);
                    p1.Position.Y = (graphics.GraphicsDevice.Viewport.Height / 2 - (p1.Texture.Height / 2)) - StringVector.Y;

                    //Random Ball position
                    baller.Position.X = R.Next(0, GraphicsDevice.Viewport.Width);
                    baller.Position.Y = R.Next(0, GraphicsDevice.Viewport.Height);

                    //Sets high score...
                    if (p1.score > HighScore)
                    {
                        HighScore = p1.score;
                        SaveAndLoad.Save(HighScore);
                    }

                    p1.score = 0;

                    Pause = true;
                }

            }

            //Sets high score...
            if (p1.score > HighScore)
            {
                HighScore = p1.score;
            }

            
            
            base.Update(gameTime);
        }

       //Draw
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            //Begin
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            /*
            //Drawn Absolute last so lines cover everything
            for (int ScreenHeight = GraphicsDevice.Viewport.Height -1; ScreenHeight > 0; ScreenHeight-=4 )
            {
                spriteBatch.Draw(VectorLines,new Vector2(GraphicsDevice.Viewport.Width- VectorLines.Width,ScreenHeight),Color.White);
            }

             */

            //Drawn last so pause is over everything
            //Says if game is paused in the middle of the screen
            if (Pause == true && gameover == false)
            {
                //Measures string to correct for screen center
                Vector2 StringVector = PauseFont.MeasureString("PAUSED");
                
                spriteBatch.DrawString(PauseFont, "PAUSED", new Vector2(((GraphicsDevice.Viewport.Width / (float)2.00) - (StringVector.X / (float)2.00)), ((GraphicsDevice.Viewport.Height / (float)2.00) - (StringVector.Y / (float)2.00))), Color.Red);
               
            }   
            else if (Pause == true && gameover == true)
            {
                    //Measures string to correct for screen center
                    Vector2 StringVector = PauseFont.MeasureString("GAME OVER");
                    //Measures High Score string
                    Vector2 HighScoreStringVector = PauseFont.MeasureString("High Score: " + HighScore);
                    Vector2 PressSpaceVector = Font.MeasureString("(Press Space To Restart)");
                    
                    spriteBatch.DrawString(PauseFont, "GAME OVER", new Vector2(((GraphicsDevice.Viewport.Width / (float)2.00) - (StringVector.X / (float)2.00)), ((GraphicsDevice.Viewport.Height / (float)2.00) - (StringVector.Y / (float)2.00))), Color.Red);
                    //Say High Score
                    spriteBatch.DrawString(PauseFont, "High Score: " + HighScore, new Vector2(((GraphicsDevice.Viewport.Width / (float)2.00) - +(HighScoreStringVector.X / 2.00f)), ((GraphicsDevice.Viewport.Height / (float)2.00) - (StringVector.Y / (float)2.00)) + HighScoreStringVector.Y), Color.Red);
                    //Draw Score
                    spriteBatch.DrawString(Font, "(Press Space To Restart)", new Vector2(((GraphicsDevice.Viewport.Width / (float)2.00)) - (PressSpaceVector.X/2.00f), ((GraphicsDevice.Viewport.Height / (float)2.00) - (StringVector.Y / (float)2.00)) + HighScoreStringVector.Y + StringVector.Y), Color.Red);
                   
            }

            if (Paddle.Mute == true)
            {
                //Measures string to correct for screen center
                Vector2 StringVector = Font.MeasureString("MUTE  ");
                spriteBatch.DrawString(Font, "MUTE  ", new Vector2(GraphicsDevice.Viewport.Width - StringVector.X,GraphicsDevice.Viewport.Height - StringVector.Y),Color.Red);
            }

            //Draw Score
            spriteBatch.DrawString (Font,"Score: " + p1.score.ToString(), new Vector2(10, 10), Color.Red);

            //Draw paddle
            p1.Draw(spriteBatch);

            //Draw the ball
            baller.Draw(spriteBatch);
            
            
            //Draw Enemies
            foreach (EnemyBall enemy in Enemies)
            {
                enemy.Draw(spriteBatch);
            }

            
            //End
            spriteBatch.End();
            
            base.Draw(gameTime);
        }



        //Player Input
        public void PlayerInput()
        {
            //If the game is not paused
            if (Pause == false)
            {
                //Player
                //up
                if (Keyboard.GetState().IsKeyDown(Keys.Up) == true || Keyboard.GetState().IsKeyDown(Keys.W) == true)
                {
                    p1.Position.Y -= p1.speed;
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                    {
                        p1.Position.Y -= p1.speed / 2.00f;
                    }
                }

                //down
                if (Keyboard.GetState().IsKeyDown(Keys.Down) == true || Keyboard.GetState().IsKeyDown(Keys.S) == true)
                {
                    p1.Position.Y += p1.speed;
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                    {
                        p1.Position.Y += p1.speed / 2.00f;
                    }
                }

                //right
                if (Keyboard.GetState().IsKeyDown(Keys.Right) == true || Keyboard.GetState().IsKeyDown(Keys.D) == true)
                {
                    p1.Position.X += p1.speed;
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                    {
                        p1.Position.X += p1.speed / 2.00f;
                    }
                }

                //left
                if (Keyboard.GetState().IsKeyDown(Keys.Left) == true || Keyboard.GetState().IsKeyDown(Keys.A) == true)
                {
                    p1.Position.X -= p1.speed;
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                    {
                        p1.Position.X -= p1.speed / 2.00f;
                    }
                }
            }

            //Check for pause
            if ((Keyboard.GetState().IsKeyDown(Keys.Space) && LastKeyboardState.IsKeyUp(Keys.Space)) || (Keyboard.GetState().IsKeyDown(Keys.P) && LastKeyboardState.IsKeyUp(Keys.P)))
            {
                if (Pause == true)
                {
                    Pause = false;
                }
                else if (Pause == false)
                {
                    Pause = true;
                }
            }

           

            //Check for mute
            if (Keyboard.GetState().IsKeyDown(Keys.M) && LastKeyboardState.IsKeyUp(Keys.M))
            {
                if  (Paddle.Mute == true)
                {
                    Paddle.Mute = false;
                }
                else
                {
                    Paddle.Mute = true;
                }
            }

            //Important stores last key stroak
            LastKeyboardState = Keyboard.GetState();
        }

        bool Collisions(Ball baller)
        {

            if (baller.CollisionBox.Intersects(p1.CollisionBox))
            {


                //Do While Needed
                do
                {
                    baller.Position.X = R.Next(0, GraphicsDevice.Viewport.Width);
                    baller.Position.Y = R.Next(0, GraphicsDevice.Viewport.Height);

                } while (baller.Position.X >= p1.Position.X - BufferZone && baller.Position.X <= p1.Position.X + p1.Texture.Width + BufferZone && baller.Position.Y >= p1.Position.Y - p1.Texture.Height - BufferZone && baller.Position.Y <= p1.Position.Y + p1.Texture.Height + BufferZone);

                if (Paddle.Mute == false)
                {
                    ding.Play();
                }



                for (int i = Difficulty; i > 0; i-- )
                {
                    //Adds Enemy
                    Enemies.Add(new EnemyBall());
                    //Give enemies textures
                    Enemies[Enemies.Count - 1].Texture = Content.Load<Texture2D>("ballred");
                    //Give enemies sound
                    Enemies[Enemies.Count - 1].Ping = Content.Load<SoundEffect>("blip");
                    //Enemy starting location
                    do
                    {
                        Enemies[Enemies.Count - 1].Position.X = R.Next(0, GraphicsDevice.Viewport.Width);
                        Enemies[Enemies.Count - 1].Position.Y = R.Next(0, GraphicsDevice.Viewport.Height);
                    } while (Enemies[Enemies.Count - 1].Position.X >= p1.Position.X - BufferZone && Enemies[Enemies.Count - 1].Position.X <= p1.Position.X + p1.Texture.Width + BufferZone && Enemies[Enemies.Count - 1].Position.Y >= p1.Position.Y - p1.Texture.Height - BufferZone && Enemies[Enemies.Count - 1].Position.Y <= p1.Position.Y + p1.Texture.Height + BufferZone);
                }
                    
                

               
                p1.score+= 1000;


                //Adds Enemy To increase difficulty
                //Enemies.Add(new EnemyBall());
                

            }

            return false;
        }

        bool Collisions(EnemyBall baller)
        {

            if (baller.CollisionBox.Intersects(p1.CollisionBox))
            {

                //Clear Enemies
                Enemies.Clear();
      
                return true;

            }

            return false;
        }
    }
}
