﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Important
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace SQUARE
{
    static class SaveAndLoad
    {

        public static void Save(int highscore)
        {
            //Declare new instance of class
            HighScoreClass HS = new HighScoreClass();

            //Set class equal to the gotten high score
            HS.SetHighScore(highscore);

            IFormatter formatter = new BinaryFormatter();

            Stream stream = new FileStream("MyFile.bin",FileMode.Create,FileAccess.Write,FileShare.None);

            formatter.Serialize(stream,HS);

            //Close Stream
            stream.Close();

        }

        public static int Load()
        {
            IFormatter formatter = new BinaryFormatter();

            try
            {
                Stream stream = new FileStream("MyFile.bin", FileMode.Open, FileAccess.Read, FileShare.Read);

                HighScoreClass HS = (HighScoreClass)formatter.Deserialize(stream);
                stream.Close();

                return HS.GetHighScore();
            }
            catch
            {
                Save(0);
            }

            return 0;
            
        }

    }
}
