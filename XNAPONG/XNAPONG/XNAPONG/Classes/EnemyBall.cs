﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Important
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;



namespace SQUARE
{
    class EnemyBall : Microsoft.Xna.Framework.Game
    {
        //Variables
        public Texture2D Texture;
        public Vector2 Position;
        public Rectangle CollisionBox;
        public Vector2 Velocity = new Vector2(200.0f,200.0f);
        public SoundEffect Ping;
        public bool enemy = true;
        float speed = 1.0f;

        //Draw
        public void Draw(SpriteBatch SB)
        {
            SB.Draw(Texture,Position,Color.White);
        }


        //Update
        public void Update(GraphicsDeviceManager GDM, GameTime GameTime,Paddle Player,Random R)
        {
            
            if (R.Next(1,500) == 5)
            {
               Velocity.X = Player.Position.X - Position.X;
               Velocity.Y = Player.Position.Y - Position.Y;
            }

            Player.score++;

            //Draws Rectangle for collisions
            CollisionBox = new Rectangle(Convert.ToInt32(Position.X), Convert.ToInt32(Position.Y), Texture.Width, Texture.Height);


            KeepInScreen(GDM, Player);
            Physics(GameTime,Player);

            

        }

        public void KeepInScreen(GraphicsDeviceManager GDM, Paddle Player)
        {
            int ScreenBottom = GDM.GraphicsDevice.Viewport.Bounds.Bottom;
            int ScreenTop = GDM.GraphicsDevice.Viewport.Bounds.Top;
            int ScreenRight = GDM.GraphicsDevice.Viewport.Bounds.Right;
            int ScreenLeft = GDM.GraphicsDevice.Viewport.Bounds.Left;

            //Y
            if (Position.Y >= ScreenBottom - Texture.Height)
            {
                Position.Y = ScreenBottom - Texture.Height;

                
                Velocity.Y *= -1;

               
            }
            if (Position.Y <= ScreenTop)
            {
                Position.Y = ScreenTop;
                
                    Velocity.Y *= -1;

                    
            }

            //X
            if (Position.X >= ScreenRight - Texture.Width)
            {
                Position.X = ScreenRight - Texture.Width;
               
                    Velocity.X *= -1;

                   
            }
            if (Position.X <= ScreenLeft)
            {
                Position.X = ScreenLeft + Texture.Width;
               
               
                    Velocity.X *= -1;

                    
            }

        }


        public void Physics(GameTime GameTime, Paddle Player)
        {
            //move the sprite by speed scaled by elapsed time
          // Position += Velocity * (float)GameTime.ElapsedGameTime.TotalSeconds;

            Position += Velocity * speed * (float)GameTime.ElapsedGameTime.TotalSeconds;
        }

        
    }
}
