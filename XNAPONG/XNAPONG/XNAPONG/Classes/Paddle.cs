﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Important
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SQUARE
{
    //Important to make it part of Microsoft.Xna.Framework.Game
   class Paddle : Microsoft.Xna.Framework.Game
    {
        //Class for paddle

        //Variables
        public  Texture2D Texture;
        public Vector2 Position = Vector2.Zero;
        public Rectangle CollisionBox;
        public int speed = 10;
        public int score = 0;
        public float rotation = 0;
        public static bool Mute = false;
        

        public void Update(GraphicsDeviceManager GDM)
        {
            //Draws Rectangle for collisions
           CollisionBox = new Rectangle(Convert.ToInt32(Position.X), Convert.ToInt32(Position.Y), Texture.Width, Texture.Height);

           
            //Keep paddle in screen
            KeepInScreen(GDM);
            
        }

        public void Draw(SpriteBatch SB)
        {
            SB.Draw(Texture,Position,Color.White);
        }

        public void KeepInScreen(GraphicsDeviceManager GDM)
        {
            int ScreenBottom = GDM.GraphicsDevice.Viewport.Bounds.Bottom;
             int ScreenTop = GDM.GraphicsDevice.Viewport.Bounds.Top;
             int ScreenRight = GDM.GraphicsDevice.Viewport.Bounds.Right;
             int ScreenLeft = GDM.GraphicsDevice.Viewport.Bounds.Left;

             if (Position.Y >= ScreenBottom - Texture.Height)
            {
                Position.Y = ScreenBottom - Texture.Height;
            }
            if (Position.Y <= ScreenTop)
            {
                Position.Y = ScreenTop;
            }

            if (Position.X <= ScreenLeft)
            {
                Position.X = ScreenLeft;
            }
            if (Position.X >= ScreenRight- Texture.Width)
            {
                Position.X = ScreenRight - Texture.Width;
            }

        }

    }
}
