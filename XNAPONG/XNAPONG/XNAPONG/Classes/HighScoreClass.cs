﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQUARE
{
    [Serializable]
    class HighScoreClass
    {
        private int HighScore;

        //Set High Score
       public void SetHighScore(int highscore)
        {
            HighScore = highscore;
        }

        //Get High Score
        public int GetHighScore()
       {
           return HighScore;
       }
    }
}
